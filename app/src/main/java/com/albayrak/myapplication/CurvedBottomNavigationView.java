package com.albayrak.myapplication;


import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;

import androidx.core.content.ContextCompat;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class CurvedBottomNavigationView extends BottomNavigationView {
    private Path mPath;
    private Paint mPaint;

    private int shadowWidth = 0;
    private int middleIconWidth = 0;
    private int paddingTop = 0;

    private Point leftStartPoint = new Point();
    private Point leftControl1 = new Point();
    private Point leftControl2 = new Point();
    private Point leftControl3 = new Point();

    private Point rightEndPoint = new Point();
    private Point rightControl1 = new Point();
    private Point rightControl2 = new Point();
    private Point rightControl3 = new Point();

    private Point bottomPoint = new Point();

    public int convertDptoPixels(float dp) {
        return (int) (dp * getContext().getResources().getDisplayMetrics().density + 0.5f);
    }

    public CurvedBottomNavigationView(Context context) {
        super(context);
        init();
    }

    public CurvedBottomNavigationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CurvedBottomNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        mPath = new Path();
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setColor(Color.WHITE);
        setBackgroundColor(Color.TRANSPARENT);
        middleIconWidth = convertDptoPixels(68);
        shadowWidth = convertDptoPixels(5);
        mPaint.setShadowLayer(15, 0, 0, Color.DKGRAY);
        setLayerType(LAYER_TYPE_SOFTWARE, mPaint);
        paddingTop = convertDptoPixels(5);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        int width = getWidth();
        int height = getHeight();

        rightEndPoint.set((width / 2) + (middleIconWidth / 2) + (shadowWidth) + convertDptoPixels(9), paddingTop);
        leftStartPoint.set((width / 2) - (middleIconWidth / 2) - (shadowWidth) - convertDptoPixels(9), paddingTop);
        bottomPoint.set(width / 2, height - convertDptoPixels(20) + paddingTop);

        leftControl1.set(leftStartPoint.x + ((shadowWidth * 3) / 2), (shadowWidth/2) + (paddingTop));
        leftControl2.set(leftStartPoint.x + ((int)(shadowWidth * 1.9)), (shadowWidth * 3) + (paddingTop));
        leftControl3.set(leftStartPoint.x + ((shadowWidth * 7) / 2), middleIconWidth - (shadowWidth * 4) + (paddingTop));

        rightControl1.set(rightEndPoint.x - ((shadowWidth * 3) / 2), (shadowWidth/2) + (paddingTop));
        rightControl2.set(rightEndPoint.x - ((int)(shadowWidth * 1.9)), (shadowWidth * 3) + (paddingTop));
        rightControl3.set(rightEndPoint.x - ((shadowWidth * 7) / 2), middleIconWidth - (shadowWidth * 4) + (paddingTop));

        mPath.reset();
        mPath.moveTo(0, paddingTop + convertDptoPixels(12));
        mPath.quadTo(convertDptoPixels(2), convertDptoPixels(2)+paddingTop, convertDptoPixels(12), paddingTop);
        mPath.lineTo(leftStartPoint.x, leftStartPoint.y);

        mPath.quadTo(leftControl1.x, leftControl1.y, leftControl2.x, leftControl2.y);
        mPath.quadTo(leftControl3.x, leftControl3.y, bottomPoint.x, bottomPoint.y);
        mPath.quadTo(rightControl3.x, rightControl3.y, rightControl2.x, rightControl2.y);
        mPath.quadTo(rightControl1.x, rightControl1.y, rightEndPoint.x, rightEndPoint.y);
        mPath.lineTo(width - convertDptoPixels(12), paddingTop);
        mPath.quadTo(width - convertDptoPixels(2), convertDptoPixels(2) + paddingTop, width, paddingTop  + convertDptoPixels(12));
        mPath.lineTo(width, height + paddingTop);
        mPath.lineTo(0, height + paddingTop);
        mPath.close();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawPath(mPath, mPaint);
    }
}